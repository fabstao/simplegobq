package main

import (
	"context"
	"fmt"

	"cloud.google.com/go/bigquery"
	"google.golang.org/api/iterator"
)

func doQuery(client *bigquery.Client, ctx context.Context) {
	q := client.Query("SELECT year, SUM(number) as num FROM bigquery-public-data.usa_names.usa_1910_2013 WHERE name = 'William' GROUP BY year ORDER BY year")
	it, err := q.Read(ctx)
	if err != nil {
		// TODO: Handle error.
		fmt.Println(err)
	}
	fmt.Printf("\n_______________________________________________________________________\n\n")
	fmt.Println("Query gets how many Williams were registered per year from 1910 to 2013")
	fmt.Printf("_______________________________________________________________________\n\n")
	for {
		var values []bigquery.Value
		err := it.Next(&values)
		if err == iterator.Done {
			break
		}
		if err != nil {
			// TODO: Handle error.
			fmt.Println(err)
		}
		fmt.Printf("Initial year: %d | Quantity of Williams: %d\n", values[0], values[1])
	}
}
