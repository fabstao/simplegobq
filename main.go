package main

import (
	"context"
	"fmt"
	"log"

	"cloud.google.com/go/bigquery"
)

func main() {
	fmt.Println("Starting...")
	projectID := "clean-authority-351915"
	ctx := context.Background()
	client, err := bigquery.NewClient(ctx, projectID)
	if err != nil {
		// TODO: Handle error.
		log.Fatalln(err)
	}
	doQuery(client, ctx)
}
