# Basic Google BigQuery Example using Go

## Introduction
This is a simple demo on how to query a BigQuery dataset.

## Google BigQuery Quickstart

Follow this tutorial in order to learn how to work with BigQuery

[https://cloud.google.com/bigquery/docs/quickstarts/load-data-bq]()

## TODO

* Make this example more interactive
* Feature: load CSV to new or existing table
