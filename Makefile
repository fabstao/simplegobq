export GO="/usr/bin/go"
export GOOS=linux
export GOARCH=amd64

all:
	mkdir -p build
	${GO} build -o build/gobq *.go

clean:
	rm -rf build/*
